// alert("Hey")


// Nested Classes
	

// Student Constructor
class Student {
	constructor(name, email, grades) {
		this.name = name;
		this.email = email;
		this.gradeAve = undefined;
		this.passed = undefined;
		this.passedWithHonors = undefined;

		if(grades.length === 4){
		     if(grades.every(grade => grade >= 0 && grade <= 100)){
		         this.grades = grades;
		     } else {
		         this.grades = undefined;
		     }
		 } else {
		     this.grades = undefined;
		 }
	}

// Student Methods

	login() {
		console.log(`${this.email} has logged in`);
		return this;
	}
	logout() {
		console.log(`${this.email} has logged out`);
		return this
	}
	listGrades(){
		console.log(`${this.name}'s quarterly grade averages are: ${this.grades}`);
		return this;
	}
	computeAve() {
		let sum = 0;
		this.grades.forEach(grade => sum = sum + grade);
		this.gradeAve = sum/4;
		return this;
	}
	willPass() {
		this.passed = this.computeAve().gradeAve >= 85 ? true : false;
		return this;
	}

	willPassWithHonors() {
		if (this.passed) {
		    if (this.gradeAve >= 90) {
		        this.passedWithHonors = true;
		    } else {
		        this.passedWithHonors = false;
		    }
		} else {
		    this.passedWithHonors = false;
		}
		return this;
	}
}





// Class to represent a section of a student

// Section Constructor
class Section {
// Constructor
	// every section object will be instantiated with an empty array for its students
	constructor(name){
		this.name = name;
		this.students = [];
		this.honorStudents = undefined;
		this.honorPercentage = undefined;
	
	}


// Section Methods
	// method for adding student to a section
	addStudent(name,email,grades){
		this.students.push(new Student(name,email,grades))

		return this
	}
	// section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88])


	// method for computing honor students in a section
	countHonorStudents(){
		let count = 0;
		this.students.forEach(student => {
			if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
				count++
			}
		})

		this.honorStudents = count;
		return this
	}
	// section1A.countHonorStudents()


	computeHonorsPercentage(){
		this.honorPercentage = (this.honorStudents/this.students.length)*100;
		return this

	}
	// section1A.countHonorStudents().computeHonorsPercentage()




}

								// name of section
// const section1A = new Section ('section1A')
// console.log(section1A)

// How to access student details
// section1A.students[0]




// Class for Grade/Level

// Grade Constructor
class Grade {
	constructor(number){
		this.number = number;
		this.sections = [];
		this.totalStudents = 0;
		this.totalHonorStudents = 0;
		this.batchAveGrade = undefined;
		this.batchMinGrade = undefined;
		this.batchMaxGrade = undefined;

	}


// Grade Methods

	addSection(name){
		this.sections.push(new Section(name))
		return this
	}
	// grade1.addSection("section1A")


	countStudents(){
		let total = 0;
		this.sections.forEach(section => {
			total += section.students.length
		})
		this.totalStudents = total;
		return this;
	}
	// grade1.countStudents()


	countHonorStudents(){
		this.sections.forEach(section =>{
			// copypasted code from section.countHonorStudents()
			section.students.forEach(student => {
						if(student.computeAve().willPass().willPassWithHonors().passedWithHonors){
							// increment if statement above is true
							this.totalHonorStudents++
						}
					})
		})
		return this;
	}
	// grade1.countStudents().countHonorStudents()


	computeBatchAve(){
	// 1
        let sectionAve = 0;
        this.sections.forEach(section => {
            section.students.forEach(student => {
                sectionAve += student.computeAve().gradeAve;
            })
        })
        this.batchAveGrade = sectionAve/this.countStudents().totalStudents;
        return this;



	// 2
		/*let sumOfGrades = 0;
		let NoOfGrades = 0;

		this.sections.forEach(section =>{
			section.students.forEach(student =>{
				sumOfGrades += student.computeAve().gradeAve;
				NoOfGrades++
			})
		})
		this.batchAveGrade = sumOfGrades/NoOfGrades;
		return this;*/
	}

	// grade1.countStudents().countHonorStudents().computeBatchAve()


	getBatchMinGrade(){
		let batchGrades = [];
	    this.sections.forEach(section => {
	      	section.students.forEach(student => {
	      		for (let i = 0; i < student.grades.length; i++){
	        		batchGrades.push(student.grades[i]);
	        	}
	      	})
	    })
	    this.batchMinGrade = Math.min.apply(Math, batchGrades);
	    return this;
	}

	// grade1.countStudents().countHonorStudents().computeBatchAve().getBatchMinGrade()

	getBatchMaxGrade(){
		let batchGrades = [];
	    this.sections.forEach(section => {
	      	section.students.forEach(student => {
	      		for (let i = 0; i < student.grades.length; i++){
	        		batchGrades.push(student.grades[i]);
	        	}
	      	})
	    })
	    this.batchMaxGrade = Math.max.apply(Math, batchGrades);
	    return this;
	}
	// grade1.countStudents().countHonorStudents().computeBatchAve().getBatchMinGrade().getBatchMaxGrade()


}





// List of Grade/Level
let grade1 = new Grade()


// Create of Sections
grade1.addSection('section1A');
grade1.addSection('section1B');
grade1.addSection('section1C');
grade1.addSection('section1D');


//List of Sections
const section1A = grade1.sections.find(section => section.name === "section1A");
const section1B = grade1.sections.find(section => section.name === "section1B");
const section1C = grade1.sections.find(section => section.name === "section1C");
const section1D = grade1.sections.find(section => section.name === "section1D");


//Populate the sections with students
section1A.addStudent('John', 'john@mail.com', [89, 84, 78, 88]);
section1A.addStudent('Joe', 'joe@mail.com', [78, 82, 79, 85]);
section1A.addStudent('Jane', 'jane@mail.com', [87, 89, 91, 93]);
section1A.addStudent('Jessie', 'jessie@mail.com', [91, 89, 92, 93]);

section1B.addStudent('Jeremy', 'jeremy@mail.com', [85, 82, 83, 89]);
section1B.addStudent('Johnny', 'johnny@mail.com', [82, 86, 77, 88]);
section1B.addStudent('Jerome', 'jerome@mail.com', [89, 85, 92, 91]);
section1B.addStudent('Janine', 'janine@mail.com', [90, 87, 94, 91]);

section1C.addStudent('Faith', 'faith@mail.com', [87, 85, 88, 91]);
section1C.addStudent('Hope', 'hope@mail.com', [85, 87, 84, 89]);
section1C.addStudent('Love', 'love@mail.com', [91, 87, 90, 88]);
section1C.addStudent('Joy', 'joy@mail.com', [92, 86, 90, 89]);

section1D.addStudent('Eddie', 'eddie@mail.com', [85, 87, 86, 92]);
section1D.addStudent('Ellen', 'ellen@mail.com', [88, 84, 86, 90]);
section1D.addStudent('Edgar', 'edgar@mail.com', [90, 89, 92, 86]);
section1D.addStudent('Eileen', 'eileen@mail.com', [90, 88, 93, 84]);


// console.log(section1B)

// Students





















// Resources
	// https://gitlab.com/tuitt/students/batch273/resources/s04/discussion/-/blob/master/index.js

// 3. grade1.sections.find(section => section.name === "section1A").addStudent('John', 'john@mail.com', [89, 84, 78, 88])